package chaakara.com.freshfish.logins;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import chaakara.com.freshfish.HomeActivity;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseActivity;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.home.ResetPassword;
import chaakara.com.freshfish.responsemodels.ForgotPassword;
import chaakara.com.freshfish.responsemodels.SignInModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static chaakara.com.freshfish.commons.Constants.INVALID_CREDENTIALS;
import static chaakara.com.freshfish.commons.Constants.LOGIN_SERVER_ERROR;
import static chaakara.com.freshfish.commons.Constants.RGISTRATION_SERVER_ERROR;

public class UserLogin extends BaseActivity implements View.OnClickListener {
    TextView login;
    @BindView(R.id.mobile_et)
    EditText phoneNumber_et;
    @BindView(R.id.password_et)
    EditText password_et;
    @BindView(R.id.forgot)
    TextView forgot;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.layout_login);
        ButterKnife.bind(this);
        login = findViewById(R.id.login);
        login.setOnClickListener(this);
        forgot.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.login) {
            if (phoneNumber_et.getText().toString().length() == 0) {
                phoneNumber_et.setError("Cannot be empty");
            } else if (password_et.getText().toString().length() == 0) {
                password_et.setError("Cannot be empty");
            } else {
                phone = phoneNumber_et.getText().toString();
                password = password_et.getText().toString();
                callApiLOgin();

            }

        } else if (view.getId() == R.id.forgot) {
            callForgot();
        }
    }

    String phone = "";
    String password = "";

    private void callForgot() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserLogin.this);
        alertDialog.setTitle("Forgot Password");
        alertDialog.setMessage("Enter Your Registered Phone number");
        final EditText input = new EditText(UserLogin.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                progress.show();
                apiInterface.forgotPassword(input.getText().toString()).enqueue(new Callback<ForgotPassword>() {
                    @Override
                    public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                        if (progress.isShowing())
                            progress.cancel();
                        if (response.isSuccessful()) {
//                            Toast.makeText(UserLogin.this, "OTP sent to your phone number", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(UserLogin.this, ResetPassword.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotPassword> call, Throwable t) {
                        if (progress.isShowing())
                            progress.cancel();
                    }
                });
            }
        });
        alertDialog.setView(input);
        alertDialog.show();
    }

    private void callApiLOgin() {
        try {
            progress.show();
            apiInterface.Login(phone, password)
                    .enqueue(new Callback<SignInModel>() {
                        @Override
                        public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                            if (progress.isShowing()) {
                                progress.cancel();
                            }
                            if (response.isSuccessful()) {
                                if (response.body().getResult().equals("Success")) {
                                    List<SignInModel.DetailsBean> data = response.body().getDetails();
                                    if (data.get(0).getType().equals("admin")) {
                                        appPrefes.SaveDataBoolean(Constants.IS_ADMIN, true);
                                        appPrefes.SaveDataBoolean(Constants.IS_LOGIN, true);
                                    } else {
                                        appPrefes.SaveDataBoolean(Constants.IS_ADMIN, false);
                                        appPrefes.SaveDataBoolean(Constants.IS_LOGIN, true);
                                    }
                                    appPrefes.SaveData(Constants.USER_NAME, data.get(0).getName());
                                    appPrefes.SaveData(Constants.PHONE_USER, data.get(0).getPhoneNumber());
                                    appPrefes.SaveData(Constants.USER_ID, data.get(0).getId());
                                    startActivity(new Intent(UserLogin.this, HomeActivity.class));
                                    finish();
                                } else {
                                    showRequestSuccessDialog("Login Failed", "Invalid Credentials", "OK", INVALID_CREDENTIALS);
                                }
                            } else {
                                showServerErrorAlert(LOGIN_SERVER_ERROR);

                            }
                        }

                        @Override
                        public void onFailure(Call<SignInModel> call, Throwable t) {
                            if (progress.isShowing()) {
                                progress.cancel();
                            }
                            showServerErrorAlert(LOGIN_SERVER_ERROR);

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            showServerErrorAlert(LOGIN_SERVER_ERROR);

        }
    }

    @Override
    public void retryApiCall(int apiCode) {
        super.retryApiCall(apiCode);
        if (apiCode == LOGIN_SERVER_ERROR) {
            callApiLOgin();
        }
    }

    @Override
    public void onSuccessDialogClick(int apiCode) {
        super.onSuccessDialogClick(apiCode);
        switch (apiCode) {
            case INVALID_CREDENTIALS:

                break;
        }
    }
}
