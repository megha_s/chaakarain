package chaakara.com.freshfish.logins;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import chaakara.com.freshfish.HomeActivity;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseActivity;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.responsemodels.UserRegistrationModel;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static chaakara.com.freshfish.commons.Constants.RGISTRATION_SERVER_ERROR;

public class UserRegistration extends BaseActivity implements View.OnClickListener {
    TextView login;
    EditText name_et, mobile_et, houseAddress_et, password_et;
    Button register_bt;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.layout_user_registration);
        login = findViewById(R.id.login);
        name_et = findViewById(R.id.name_et);
        mobile_et = findViewById(R.id.mobile_et);
        houseAddress_et = findViewById(R.id.houseAddress_et);
        password_et = findViewById(R.id.password_et);
        register_bt = findViewById(R.id.register_bt);
        login.setOnClickListener(this);
        register_bt.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.login) {
            startActivity(new Intent(UserRegistration.this, UserLogin.class));
            finish();
        }
        if (view.getId() == R.id.register_bt) {
            if (isEmpty(name_et)) {
                toast("Name Cannot be empty");
            } else if (isEmpty(mobile_et)) {
                toast("Phone number Cannot be empty");
            } else if (isEmpty(houseAddress_et)) {
                toast("House address Cannot be empty");
            } else if (isEmpty(password_et)) {
                toast("Password Cannot be empty");
            } else {
                name = name_et.getText().toString();
                phone = mobile_et.getText().toString();
                address = houseAddress_et.getText().toString();
                password = password_et.getText().toString();
                callRegister();
            }
        }
    }

    String name = "";
    String phone = "";
    String address = "";
    String password = "";

    private void callRegister() {
        try {
            Map<String, RequestBody> map = new HashMap<>();
      /*  if (profilePic != null) {
            RequestBody file = RequestBody.create(MediaType.parse("image/*"), profilePic);
            map.put("image", file);
//            map.put("Profilepic\"; filename=\"ProfilePic_" + appPrefes.getData("UserID") + ".png\" ", file);
        }*/
            RequestBody nameRQ = RequestBody.create(MediaType.parse("text/plain"), name);
            map.put("name", nameRQ);
            RequestBody phonerq = RequestBody.create(MediaType.parse("text/plain"), phone);
            map.put("phoneNumber", phonerq);
            RequestBody addressrq = RequestBody.create(MediaType.parse("text/plain"), address);
            map.put("address", addressrq);
            RequestBody passwrdrq = RequestBody.create(MediaType.parse("text/plain"), password);
            map.put("password", passwrdrq);
            progress.show();
            apiInterface.register(map).enqueue(new Callback<UserRegistrationModel>() {
                @Override
                public void onResponse(Call<UserRegistrationModel> call, Response<UserRegistrationModel> response) {
                    if (progress.isShowing()) {
                        progress.cancel();
                    }
                    if (response.isSuccessful()) {
                        if (response.body().getResult().equals("Success")) {
                            List<UserRegistrationModel.DetailsBean> data = response.body().getDetails();

                            appPrefes.SaveDataBoolean(Constants.IS_ADMIN, false);
                            appPrefes.SaveDataBoolean(Constants.IS_LOGIN, true);
                            appPrefes.SaveData(Constants.USER_NAME, data.get(0).getName());
                            appPrefes.SaveData(Constants.PHONE_USER, data.get(0).getPhoneNumber());
                            appPrefes.SaveData(Constants.USER_ID, data.get(0).getUserid());
                            startActivity(new Intent(UserRegistration.this, HomeActivity.class));
                            finish();
                        } else {
                            Toast.makeText(UserRegistration.this, response.body().getMessage() + "", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        showServerErrorAlert(RGISTRATION_SERVER_ERROR);
                    }

                }

                @Override
                public void onFailure(Call<UserRegistrationModel> call, Throwable t) {
                    if (progress.isShowing()) {
                        progress.cancel();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            showServerErrorAlert(RGISTRATION_SERVER_ERROR);


        }
    }

    @Override
    public void retryApiCall(int apiCode) {
        super.retryApiCall(apiCode);
        switch (apiCode) {
            case RGISTRATION_SERVER_ERROR:
                callRegister();
        }
    }
}
