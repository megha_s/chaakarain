package chaakara.com.freshfish.commons;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import static chaakara.com.freshfish.commons.Constants.PERMISSION_REQUEST_CODE;


/**
 * Created by sics on 10/14/2018.
 */

public class PermissionUtil {


    public boolean hasStoragePermission(Context context) {
        int writePermissionCheck = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermissionCheck = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        return !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && (writePermissionCheck == PackageManager.PERMISSION_DENIED
                || readPermissionCheck == PackageManager.PERMISSION_DENIED));
    }

    public boolean hasCameraPermission(Context context) {
        int cameraPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        return !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && cameraPermissionCheck == PackageManager.PERMISSION_DENIED);
    }



    public boolean validateGrantedPermissions(int[] grantResults) {
        boolean isGranted = true;
        if (grantResults != null && grantResults.length > 0) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    isGranted = false;
                    break;
                }
            }
            return isGranted;
        } else {
            isGranted = false;
            return isGranted;
        }
    }

    public void requestMultiplePermissions(AppCompatActivity context) {
        ActivityCompat.requestPermissions(context, new String[]{
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                },
                PERMISSION_REQUEST_CODE);
    }

    public boolean shouldNeedMultiplePermission(Context context) {
        boolean status = false;
        if (
                !(hasStoragePermission(context))
                        || !(hasCameraPermission(context))
                ) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }

    public boolean shouldNeedMultiplePermissionForProfile(Context context) {
        boolean status = false;
        if (!(hasCameraPermission(context))
                || !(hasStoragePermission(context))
                ) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }

    public void requestMultiplePermissionsForProfile(AppCompatActivity context) {
        ActivityCompat.requestPermissions(context, new String[]{
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                },
                PERMISSION_REQUEST_CODE);
    }


}
