package chaakara.com.freshfish.commons;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.jpardogo.android.googleprogressbar.library.GoogleProgressBar;
import com.jpardogo.android.googleprogressbar.library.NexusRotationCrossDrawable;

import chaakara.com.freshfish.R;
import chaakara.com.freshfish.apis.APIClient;
import chaakara.com.freshfish.apis.APIInterface;
import chaakara.com.freshfish.commons.progress.MyProgressDialog;


public abstract class BaseFragment extends Fragment {
    public AppPreferences appPrefes;
    public ConnectionDetector cd;
    public AlertDialog progress;
    public APIInterface apiInterface;
    public PermissionUtil permissionUtil;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPrefes = new AppPreferences(getActivity(), getResources().getString(R.string.app_name));
//        permissionUtil = new PermissionUtil();
        apiInterface = APIClient.getClient().create(APIInterface.class);

        cd = new ConnectionDetector(getActivity());
        initDialog();
        permissionUtil = new PermissionUtil();

    }

    MyProgressDialog myProgressDialog;
    ProgressDialog progressDialog;


    private void initDialog() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_progressbar, null);
        GoogleProgressBar progressBar = (GoogleProgressBar) view.findViewById(R.id.google_progress);
        Drawable progressDrawable = new NexusRotationCrossDrawable.Builder(getActivity())
                .colors(getProgressDrawableColors())
                .build();
        progressBar.setIndeterminateDrawable(progressDrawable);
        progressBar.setIndeterminate(true);
        progress = new AlertDialog.Builder(getActivity())
                .setView(view)
                .create();

        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private int[] getProgressDrawableColors() {
        int[] colors = new int[4];
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        colors[0] = getResources().getColor(R.color.colorPrimary);
        colors[1] = getResources().getColor(R.color.colorAccent);
        colors[2] = getResources().getColor(R.color.colorPrimary);
        colors[3] = getResources().getColor(R.color.colorAccent);
        return colors;
    }
    public void addFragment(Fragment fragment, boolean addToBackStack,
                            int transition, String name, Context context) {
//
//		FragmentTransaction ft = ((FragmentActivity) context)
//				.getSupportFragmentManager().beginTransaction();
//		ft.add(R.id.container, fragment);
//		ft.setTransition(transition);
//		if (addToBackStack)
//			ft.addToBackStack(name);
//		ft.commit();
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack,
                                int transition, String name, Context context) {

//		FragmentTransaction ft = ((FragmentActivity) context)
//				.getSupportFragmentManager().beginTransaction();
//		ft.replace(R.id.container, fragment);
//		ft.setTransition(transition);
//		if (addToBackStack)
//			ft.addToBackStack(name);
//		ft.commit();
    }


    public void showNoInternetAlert(Context context, final int apiCode) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_no_internet);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final TextView tvRetry = (TextView) dialog.findViewById(R.id.tvRetry);
        final TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                retryApiCall(apiCode);
            }
        });
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showServerErrorAlert(Context context, final int apiCode) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_server_error);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final TextView tvRetry = (TextView) dialog.findViewById(R.id.tvRetry);
        final TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                retryApiCall(apiCode);
            }
        });
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showRequestSuccessDialog(Context context, String title, String message, String button, final int code) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_popup);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        final TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        tvTitle.setText(title);
        tvMessage.setText(message);
        tvCancel.setText(button);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSuccessDialogClick(code);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showAlertDialog(Context context, String title, String message, String okButton, String cancelButton, final int code) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        final TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        final TextView tvOk = (TextView) dialog.findViewById(R.id.tvOk);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        tvTitle.setText(title);
        tvMessage.setText(message);
        tvOk.setText(okButton);
        tvCancel.setText(cancelButton);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAlertOkButton(code);
                dialog.dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onClickAlertOkButton(int apiCode) {

    }

    public void onSuccessDialogClick(int apiCode) {

    }

    public void retryApiCall(int apiCode) {

    }


}
