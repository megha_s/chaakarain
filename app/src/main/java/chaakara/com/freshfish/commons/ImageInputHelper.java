package chaakara.com.freshfish.commons;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.models.Image;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import chaakara.com.freshfish.GetFilePath;

import static com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES;
import static com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT;
import static com.darsh.multipleimageselect.helpers.Constants.REQUEST_CODE;

/**
 * Class that helps, for Android App, selecting image from gallery, getting image from camera and
 * cropping image.
 * <p/>
 * <p/>
 * IMPORTANT: The Activity, that contains this object or that contains the fragment that uses this
 * object, must handle the orientation change during taking photo. Either lock the orientation of
 * the Activity or handle orientation changes. Otherwise taking photo feature will not work since
 * the new instance of this object will be created when device rotates.
 */
public class ImageInputHelper {
    private Uri picUri;
    private File profilePic = null;
    private static final int REQUEST_PICTURE_FROM_GALLERY = 23;
    private static final int REQUEST_PICTURE_FROM_CAMERA = 24;
    private String currentImageSring = "";

    GetFilePath getFilePath;

    /**
     * Activity object that will be used while calling startActivityForResult(). Activity then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImageInputHelper for handling result and being notified.
     */
    private AppCompatActivity mContext;

    /**
     * Fragment object that will be used while calling startActivityForResult(). Fragment then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImageInputHelper for handling result and being notified.
     */

    /**
     * Listener instance for callbacks on user events. It must be set to be able to use
     * the ImageInputHelper object.
     */
    private ImageActionListener imageActionListener;

    public ImageInputHelper(AppCompatActivity mContext) {
        this.mContext = mContext;
    }

    public void setImageActionListener(ImageActionListener imageActionListener) {
        this.imageActionListener = imageActionListener;
    }

    /**
     * Handles the result of events that the Activity or Fragment receives on its own
     * onActivityResult(). This method must be called inside the onActivityResult()
     * of the container Activity or Fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((resultCode == Activity.RESULT_OK) && (requestCode == REQUEST_PICTURE_FROM_CAMERA)) {
//                try {
            File getImage = new File(android.os.Environment.getExternalStorageDirectory() + File.separator + currentImageSring + ".jpg");
//                    picUri = Uri.fromFile(getImage);
//                    Bitmap myBitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), picUri);
//                    myBitmap = rotateImageIfRequired(myBitmap, picUri);
//                    myBitmap = getResizedBitmap(myBitmap, 500);
//                    profilePic = savetoFile(myBitmap);
//                    imageActionListener.onImageTakenFromCamera(profilePic);
            UCrop.Options options = new UCrop.Options();
            options.setFreeStyleCropEnabled(false);

            UCrop.of(Uri.fromFile(getImage), Uri.fromFile(getImage))
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(800, 800)
                    .start(mContext);

        }
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            ArrayList<Image> images = data.getParcelableArrayListExtra(INTENT_EXTRA_IMAGES);

            for (int i = 0; i < images.size(); i++) {
//                Uri uri = Uri.fromFile(new File(images.get(i).path));
//                File f = new File(uri));
                Uri uri = Uri.fromFile(new File(images.get(i).path));
                imageActionListener.onImageSelectedFromGallery(images.get(i).path);
                // start play with image uri
            }
        }

        /*else if ((resultCode == Activity.RESULT_OK) && (requestCode == REQUEST_PICTURE_FROM_GALLERY)) {
            try {
                picUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = mContext.getContentResolver().query(picUri,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

//                imageActionListener.onImageSelectedFromGallery(BitmapFactory.decodeFile(picturePath));

                File f = new File(picturePath);
                imageActionListener.onImageSelectedFromGallery(f);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        else if (resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            getFilePath = new GetFilePath(mContext);
            final Uri resultUri = UCrop.getOutput(data);
            imageActionListener.onImageTakenFromCamera(getFilePath.getPath(resultUri));

        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }

    /**
     * Starts an intent for selecting image from gallery. The result is returned to the
     * onImageSelectedFromGallery() method of the ImageSelectionListener interface.
     */
    public void selectImageFromGallery() {
        Intent intent = new Intent(mContext, AlbumSelectActivity.class);
        intent.putExtra(INTENT_EXTRA_LIMIT, 1); // set limit for image selection
        mContext.startActivityForResult(intent, REQUEST_CODE);
       /* checkListener();

        try {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + currentImageSring + ".jpg");
            picUri = Uri.fromFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        mContext.startActivityForResult(intent, REQUEST_PICTURE_FROM_GALLERY);
*/
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public File savetoFile(Bitmap bitmap) {
        FileOutputStream ostream;
        File file = null;
        try {
            file = File.createTempFile("Edited", "png", mContext.getApplicationContext().getExternalCacheDir());
            file.createNewFile();
            ostream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ostream.flush();
            ostream.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return file;
    }

    /**
     * Starts an intent for taking photo with camera. The result is returned to the
     * onImageTakenFromCamera() method of the ImageSelectionListener interface.
     */
    public void takePhotoWithCamera() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mContext,
                    new String[]{Manifest.permission.CAMERA}, 100);
        } else {
            /*Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + currentImageSring + ".jpg");
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            mContext.startActivityForResult(cameraIntent, REQUEST_PICTURE_FROM_CAMERA);*/
/*

            Intent getImage = new Intent(Intent.ACTION_VIEW);
            getImage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            Uri picUri = FileProvider.getUriForFile(
                    mContext,
                    mContext.getApplicationContext()
                            .getPackageName() + ".provider", file);
            getImage.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
//            install.setDataAndType(apkURI, mimeType);
            getImage.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
*/


            File file = new File(android.os.Environment.getExternalStorageDirectory() + File.separator + currentImageSring + ".jpg");
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            } else {
                Uri photoUri = FileProvider.getUriForFile(mContext.getApplicationContext(), mContext.getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            }
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (intent.resolveActivity(mContext.getApplicationContext().getPackageManager()) != null) {
                mContext.startActivityForResult(intent, REQUEST_PICTURE_FROM_CAMERA);
            }

        }


    }


    private void checkListener() {
        if (imageActionListener == null) {
            throw new RuntimeException("ImageSelectionListener must be set before calling openGalleryIntent(), openCameraIntent() or requestCropImage().");
        }
    }

    public void callChooser(String currentImageSring) {
        this.currentImageSring = currentImageSring;
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Choose From");
        builder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhotoWithCamera();
            }
        });
        builder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectImageFromGallery();
            }
        });
        builder.show();
    }

    public interface ImageActionListener {
        void onImageSelectedFromGallery(String imageUri);
//        void onImageSelectedFromGallery(Bitmap imageFile);

        void onImageTakenFromCamera(String filePath);

    }
}