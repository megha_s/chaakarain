package chaakara.com.freshfish.commons;

/**
 * Created by Lijo Mathew Theckanal on 06-Apr-18.
 */

public class Constants {

    public static final String IS_LOGIN = "islogin";
    public static final String IS_ADMIN = "isAdmin";
    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String PHONE_USER = "PHONE_USER";

    /*----------------------------------------------TEST URLS-----------------------------------------------------*/
    public static final String BASE_URL = "https://chaakara.in/api/index.php/";
    public static final String IMAGE_URL = "https://chaakara.in/";

    public static final int PERMISSION_REQUEST_CODE = 1;
    public static final int INVALID_CREDENTIALS = 2;
    public static final int ITEM_ADDED_SUCCESSFULLY = 3;
    public static final String HELPLINE_SHAMMAS = "+919447441211";
//    megha123 / chaala1kg
//chaakaraapp@gmail.com

    public static final String HELPLINE_AJMAL = "+918848799332";
    //    public static final String HELPLINE_SUHAIL = "+918547208173";
    public static final int DIALOG_LOGOUT = 4;
    public static final int SUCCESS_FEEDBACK = 5;
    public static final int ENQUIRY = 6;
    public static final int RGISTRATION_SERVER_ERROR = 7;
    public static final int LOGIN_SERVER_ERROR = 8;
    public static final String LAST_CALL = "lastcall";


}
