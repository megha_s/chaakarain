package chaakara.com.freshfish.commons;

import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


    public String getTime(String dateS) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";//2018-10-01T10:03:21.055Z
        String outputPattern = "dd/MM/yyyy";//2018-09-02T00:00:00
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateS);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String formatMonth(int year, int month) {
        String formatedString = "";
        if (month <= 9) {
            formatedString = year + "0" + month;
        } else {
            formatedString = year + "" + month;
        }
        return formatedString;
    }

    public static String formatMonthwithday(int year, int month, int day) {
        String formatedString = "";
        String formatedday = "";
        if (day < 10) {
            formatedday = "0" + day;
        } else {
            formatedday = "" + day;

        }
        if (month <= 9) {
            formatedString = year + "0" + month + "" + formatedday;
        } else {
            formatedString = year + "" + month + "" + formatedday;
        }

        return formatedString;
    }

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";//2018-10-01T10:03:21.055Z
        String outputPattern = "yyyy-MM-dd'T'HH:mm:ss";//2018-09-02T00:00:00
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String convertAttendancedate(String date1) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";//2018-10-01T10:03:21.055Z
        String outputPattern = "dd/MM/yyyy";//2018-09-02T00:00:00
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(date1);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void createCalendarEvent(Context mContext, String dtstart, String title, String comment) {
        try {
            Date date1 = null;
            try {
                date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dtstart);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");

            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, date1.getTime());
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, date1.getTime());
            intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false);
            intent.putExtra(CalendarContract.Events.TITLE, title);
            intent.putExtra(CalendarContract.Events.DESCRIPTION, comment);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            mContext.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dtstart);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Set up the event data and insert it
        Resources res = mContext.getResources();
        ContentResolver cr = mContext.getContentResolver();
        ContentValues values = new ContentValues();
        TimeZone timeZone = TimeZone.getDefault();
        values.put(CalendarContract.Events.DTSTART, date1.getTime());
        values.put(CalendarContract.Events.DTEND, date1.getTime());
        values.put(CalendarContract.Events.ALL_DAY, false);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
        values.put(CalendarContract.Events.TITLE, title);
        values.put(CalendarContract.Events.DESCRIPTION, comment);
        values.put(CalendarContract.Events.CALENDAR_ID, 3); // The calendar ID 3 is from the API examples itself but no explanation given.
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        Toast.makeText(mContext, "Event Addded to Calendar", Toast.LENGTH_LONG).show();

        long eventID = Long.parseLong(uri.getLastPathSegment());
*/
    }

    public static void toast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    public static String splitforlast(String str) {
        String[] splited = str.split("\\s+");
        return splited[splited.length - 1];
    }


}
