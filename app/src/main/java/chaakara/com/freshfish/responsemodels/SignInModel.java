package chaakara.com.freshfish.responsemodels;

import java.util.List;

public class SignInModel {

    /**
     * result : Success
     * message : Login successfull.
     * details : [{"id":"2","type":"admin","name":"","phoneNumber":"8848799332","address":"","profilePic":""}]
     */

    private String result;
    private String message;
    private List<DetailsBean> details;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsBean> details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * id : 2
         * type : admin
         * name :
         * phoneNumber : 8848799332
         * address :
         * profilePic :
         */

        private String id;
        private String type;
        private String name;
        private String phoneNumber;
        private String address;
        private String profilePic;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }
    }
}
