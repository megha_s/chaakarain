package chaakara.com.freshfish.responsemodels;

import java.util.List;

public class UserRegistrationModel {
    /**
     * result : Success
     * message : User registration is Successfull.
     * details : [{"userid":"8","name":"Lena","phoneNumber":"9874550000","address":"14,Cottage Street,Denver","profilePic":""}]
     */

    private String result;
    private String message;
    private List<DetailsBean> details;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsBean> details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * userid : 8
         * name : Lena
         * phoneNumber : 9874550000
         * address : 14,Cottage Street,Denver
         * profilePic :
         */

        private String userid;
        private String name;
        private String phoneNumber;
        private String address;
        private String profilePic;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }
    }
}
