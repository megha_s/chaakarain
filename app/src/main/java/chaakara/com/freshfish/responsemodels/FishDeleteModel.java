package chaakara.com.freshfish.responsemodels;

public class FishDeleteModel {
    /**
     * result : true
     * message : Deleted item succussfully.
     */

    private boolean result;
    private String message;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
