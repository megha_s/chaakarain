package chaakara.com.freshfish.responsemodels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class FishDetailsMOdel {
    /**
     * result : Success
     * message : Successfully displayed items.
     * details : [{"fish_id":"2","fish_name":"sample 1","quantity":"2kg","image1":"http://chaakara.in/api/item/12919014641553235807.jpg","image2":"http://chaakara.in/api/item/16042563171553235807.jpeg","image3":"http://chaakara.in/api/item/17682296681553235807.jpeg","image4":"http://chaakara.in/api/item/1350986431553235807.png"},{"fish_id":"3","fish_name":"sample 2","quantity":"2kg","image1":"http://chaakara.in/api/item/8440228251553235828.png","image2":"http://chaakara.in/api/item/10676401681553235828.png","image3":"http://chaakara.in/api/item/12103113931553235828.jpeg","image4":"http://chaakara.in/api/item/21221338251553235828.png"},{"fish_id":"8","fish_name":"sample 2","quantity":"2kg","image1":"http://chaakara.in/api/item/8750857041553236451.png","image2":"http://chaakara.in/api/item/2833789471553236451.png","image3":"http://chaakara.in/api/item/logo_white.png","image4":"http://chaakara.in/api/item/logo_white.png"}]
     */

    private String result;
    private String message;
    private List<DetailsBean> details;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsBean> details) {
        this.details = details;
    }

    public static class DetailsBean implements Parcelable {
        /**
         * fish_id : 2
         * fish_name : sample 1
         * quantity : 2kg
         * image1 : http://chaakara.in/api/item/12919014641553235807.jpg
         * image2 : http://chaakara.in/api/item/16042563171553235807.jpeg
         * image3 : http://chaakara.in/api/item/17682296681553235807.jpeg
         * image4 : http://chaakara.in/api/item/1350986431553235807.png
         */

        private String fish_id;
        private String fish_name;
        private String quantity;
        private String image1;
        private String image2;
        private String image3;
        private String image4;

        protected DetailsBean(Parcel in) {
            fish_id = in.readString();
            fish_name = in.readString();
            quantity = in.readString();
            image1 = in.readString();
            image2 = in.readString();
            image3 = in.readString();
            image4 = in.readString();
        }

        public static final Creator<DetailsBean> CREATOR = new Creator<DetailsBean>() {
            @Override
            public DetailsBean createFromParcel(Parcel in) {
                return new DetailsBean(in);
            }

            @Override
            public DetailsBean[] newArray(int size) {
                return new DetailsBean[size];
            }
        };

        public String getFish_id() {
            return fish_id;
        }

        public void setFish_id(String fish_id) {
            this.fish_id = fish_id;
        }

        public String getFish_name() {
            return fish_name;
        }

        public void setFish_name(String fish_name) {
            this.fish_name = fish_name;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getImage1() {
            return image1;
        }

        public void setImage1(String image1) {
            this.image1 = image1;
        }

        public String getImage2() {
            return image2;
        }

        public void setImage2(String image2) {
            this.image2 = image2;
        }

        public String getImage3() {
            return image3;
        }

        public void setImage3(String image3) {
            this.image3 = image3;
        }

        public String getImage4() {
            return image4;
        }

        public void setImage4(String image4) {
            this.image4 = image4;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(fish_id);
            dest.writeString(fish_name);
            dest.writeString(quantity);
            dest.writeString(image1);
            dest.writeString(image2);
            dest.writeString(image3);
            dest.writeString(image4);
        }
    }
}
