package chaakara.com.freshfish.responsemodels;

import java.util.List;

public class RatingResponse {
    /**
     * result : Success
     * message : Rating added successfully.
     * details : [{"rate_id":"10","userid":"2","fish_id":"0","rating":"2","feedback":"feedback"}]
     */

    private String result;
    private String message;
    private List<DetailsBean> details;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DetailsBean> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsBean> details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * rate_id : 10
         * userid : 2
         * fish_id : 0
         * rating : 2
         * feedback : feedback
         */

        private String rate_id;
        private String userid;
        private String fish_id;
        private String rating;
        private String feedback;
        private String phone;
        private String profilePic;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private String name;

        public String getRate_id() {
            return rate_id;
        }

        public void setRate_id(String rate_id) {
            this.rate_id = rate_id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getFish_id() {
            return fish_id;
        }

        public void setFish_id(String fish_id) {
            this.fish_id = fish_id;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getFeedback() {
            return feedback;
        }

        public void setFeedback(String feedback) {
            this.feedback = feedback;
        }
    }
}
