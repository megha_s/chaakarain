package chaakara.com.freshfish.responsemodels;

import java.util.List;

public class ForgotPassword {
    /**
     * result : Success
     * message : Successfully send otp.
     * details : []
     */

    private String result;
    private String message;
    private List<?> details;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<?> getDetails() {
        return details;
    }

    public void setDetails(List<?> details) {
        this.details = details;
    }
}
