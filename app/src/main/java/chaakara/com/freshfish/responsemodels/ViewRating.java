package chaakara.com.freshfish.responsemodels;

import java.util.List;

public class ViewRating {
    /**
     * result : Success
     * message : Successfully displayed rating.
     * details : [[{"rate_id":"1","userid":"1","fish_id":"2","rating":"3","feedback":""},{"rate_id":"2","userid":"2","fish_id":"3","rating":"3","feedback":""},{"rate_id":"3","userid":"2","fish_id":"8","rating":"2","feedback":""},{"rate_id":"4","userid":"2","fish_id":"8","rating":"2","feedback":""},{"rate_id":"5","userid":"2","fish_id":"0","rating":"2","feedback":""}]]
     */

    private String result;
    private String message;
    private List<List<DetailsBean>> details;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<DetailsBean>> getDetails() {
        return details;
    }

    public void setDetails(List<List<DetailsBean>> details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * rate_id : 1
         * userid : 1
         * fish_id : 2
         * rating : 3
         * feedback :
         */

        private String rate_id;
        private String userid;
        private String fish_id;
        private String rating;
        private String feedback;

        public String getRate_id() {
            return rate_id;
        }

        public void setRate_id(String rate_id) {
            this.rate_id = rate_id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getFish_id() {
            return fish_id;
        }

        public void setFish_id(String fish_id) {
            this.fish_id = fish_id;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getFeedback() {
            return feedback;
        }

        public void setFeedback(String feedback) {
            this.feedback = feedback;
        }
    }
}
