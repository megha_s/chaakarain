package chaakara.com.freshfish;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import chaakara.com.freshfish.commons.BaseActivity;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.home.Helpine;
import chaakara.com.freshfish.home.HomeFragment;
import chaakara.com.freshfish.home.RatingAndFeedBack;
import chaakara.com.freshfish.home.ViewRatingAndFeedback;
import chaakara.com.freshfish.logins.UserLogin;

import static chaakara.com.freshfish.commons.Constants.DIALOG_LOGOUT;

public class HomeActivity extends BaseActivity {
    private static final float END_SCALE = 0.9f;

    @BindView(R.id.navigationView)
    NavigationView navigationView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.contentView)
    FrameLayout contentView;


    LinearLayout llHomeNav, llSettings, llHelpline, llLogout, llRateUs;
    TextView tv_name;
    CircleImageView profileImage;
    private String selectedFragment = "HomeFragment";
    HomeFragment homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        ButterKnife.bind(this);
        setNavigationMenus();
        homeFragment = HomeFragment.newInstance(0);
        delayFlow(homeFragment, "HomeFragment");
        changeMenuBackgroundColor();
    }

    private void setNavigationMenus() {
        setupNavigationDrawer();
        setupNavigationMenu();
        disableNavigationViewScrollbars(navigationView);
    }

    @OnClick(R.id.sideMenuLeft)
    public void sideMenuLeftClick() {
        drawerLayout.openDrawer(navigationView);
    }

    //  Wait for drawer to close
    private void delayFlow(final Fragment fragment, final String name) {
        replaceFragment(fragment, false, FragmentTransaction.TRANSIT_ENTER_MASK, name);
    }

    private void replaceFragment(Fragment fragment, boolean addToBackStack, int transition, String name) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
//        fragmentTransaction.setTransition(transition);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(name);
        }
        fragmentTransaction.commit();
    }


    private void setupNavigationDrawer() {
        drawerLayout.setBackgroundColor(getResources().getColor(R.color.white));
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        drawerLayout.setDrawerShadow(R.color.transparent, Gravity.RIGHT);
        drawerLayout.setDrawerElevation(0);
        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                                           @Override
                                           public void onDrawerSlide(View drawerView, float slideOffset) {
                                               // Scale the View based on current slide offset
                                               final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                                               final float offsetScale = 1 - diffScaledOffset;
                                               contentView.setScaleX(offsetScale);
                                               contentView.setScaleY(offsetScale);
                                               // Translate the View, accounting for the scaled width
                                               final float xOffset = drawerView.getWidth() * slideOffset;
                                               final float xOffsetDiff = contentView.getWidth() * diffScaledOffset / 2;
                                               final float xTranslation = xOffset - xOffsetDiff;
                                               contentView.setTranslationX(xTranslation);
                                               contentView.setElevation(20);
                                           }

                                           @Override
                                           public void onDrawerClosed(View drawerView) {
                                           }
                                       }

        );
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    private void setupNavigationMenu() {
        View headerView = navigationView.getHeaderView(0);
        profileImage = (CircleImageView) headerView.findViewById(R.id.profileImage);

        try {
            Picasso.with(this)
                    .load(R.drawable.default_pic)
                    .into(profileImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_name = (TextView) headerView.findViewById(R.id.tv_name);

        try {
            tv_name.setText(appPrefes.getData(Constants.USER_NAME) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        llHomeNav = (LinearLayout) headerView.findViewById(R.id.llHomeNav);
        llSettings = (LinearLayout) headerView.findViewById(R.id.settings);
        llHelpline = (LinearLayout) headerView.findViewById(R.id.helpline);
        llLogout = (LinearLayout) headerView.findViewById(R.id.logout);
        llRateUs = (LinearLayout) headerView.findViewById(R.id.llRateUs);

        llHomeNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                selectedFragment = "HomeFragment";
                changeMenuBackgroundColor();
            }
        });
        llRateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                selectedFragment = "Rating";
                changeMenuBackgroundColor();

            }
        });
        llHelpline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                selectedFragment = "HelpLine";
                changeMenuBackgroundColor();

            }
        });
        llLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog("Logout", "Do you really want log out?", "Logout", "Cancel", DIALOG_LOGOUT);

            }
        });
    }

    private void changeMenuBackgroundColor() {
        llHomeNav.setBackgroundColor(getResources().getColor(R.color.transparent));
        llSettings.setBackgroundColor(getResources().getColor(R.color.transparent));
        llHelpline.setBackgroundColor(getResources().getColor(R.color.transparent));
        llLogout.setBackgroundColor(getResources().getColor(R.color.transparent));
        llRateUs.setBackgroundColor(getResources().getColor(R.color.transparent));

        if (selectedFragment.equals("HomeFragment")) {
            delayFlow(homeFragment, "HomeFragment");
            llHomeNav.setBackgroundColor(getResources().getColor(R.color.transb));
        } else if (selectedFragment.equals("Rating")) {
            if (appPrefes.getDataBoolean(Constants.IS_ADMIN)) {
                ViewRatingAndFeedback feedback = ViewRatingAndFeedback.newInstance(0);
                delayFlow(feedback, "Rating");

            } else {
                delayFlow(new RatingAndFeedBack(), "Rating");
            }
            llRateUs.setBackgroundColor(getResources().getColor(R.color.transb));
        } else if (selectedFragment.equals("HelpLine")) {
            delayFlow(new Helpine(), "HelpLine");
            llHelpline.setBackgroundColor(getResources().getColor(R.color.transb));
        }

    }

    @Override
    public void onClickAlertOkButton(int apiCode) {
        super.onClickAlertOkButton(apiCode);
        if (apiCode == DIALOG_LOGOUT) {
            appPrefes.clearData();
            startActivity(new Intent(HomeActivity.this, UserLogin.class));
            finish();
        }
    }
}
