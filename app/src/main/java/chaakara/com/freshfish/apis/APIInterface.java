package chaakara.com.freshfish.apis;


import java.util.Map;

import chaakara.com.freshfish.responsemodels.FishDeleteModel;
import chaakara.com.freshfish.responsemodels.FishDetailsMOdel;
import chaakara.com.freshfish.responsemodels.ForgotPassword;
import chaakara.com.freshfish.responsemodels.RatingResponse;
import chaakara.com.freshfish.responsemodels.SignInModel;
import chaakara.com.freshfish.responsemodels.UserRegistrationModel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * Created by sics on 10/12/2018.
 */

public interface APIInterface {
    @FormUrlEncoded
    @POST("login")
    Call<SignInModel> Login(@Field("phoneNumber") String phonenumber, @Field("password") String password);


    @Multipart
    @POST("user_reg")
    Call<UserRegistrationModel> register(@PartMap Map<String, RequestBody> params);

    @POST("user_homePage")
    Call<FishDetailsMOdel> getAllDetails();

    @FormUrlEncoded
    @POST("rating")
    Call<RatingResponse> sendFeedback(@Field("userid") String userid, @Field("rating") String rating,
                                      @Field("feedback") String feedback);

    @POST("view_rating")
    Call<RatingResponse> viewFeedback();

    @FormUrlEncoded
    @POST("forgot_password")
    Call<ForgotPassword> forgotPassword(@Field("phone") String phone);


    @FormUrlEncoded
    @POST("change_password")
    Call<ForgotPassword> changePassword(@Field("phone") String phone, @Field("otp") String otp,
                                        @Field("new_password") String new_password);

    @FormUrlEncoded
    @POST("delete_item")
    Call<FishDeleteModel> deleteFish(@Field("fish_id") String fish_id);

    @FormUrlEncoded
    @POST("enquiry")
    Call<ForgotPassword> enquiry(@Field("userid") String userid, @Field("fishid") String fish_id);


    @Multipart
    @POST("add_item")
    Call<UserRegistrationModel> addFish(@PartMap Map<String, RequestBody> params);


    @Multipart
    @POST("edit_item")
    Call<UserRegistrationModel> editFish(@PartMap Map<String, RequestBody> params);

}

