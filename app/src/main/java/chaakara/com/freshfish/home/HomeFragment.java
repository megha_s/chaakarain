package chaakara.com.freshfish.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseFragment;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.responsemodels.FishDeleteModel;
import chaakara.com.freshfish.responsemodels.FishDetailsMOdel;
import chaakara.com.freshfish.responsemodels.ForgotPassword;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static chaakara.com.freshfish.commons.Constants.ENQUIRY;

public class HomeFragment extends BaseFragment implements View.OnClickListener, ItemsAdapter.cliclCalls {

    /**
     * Create a new instance of DetailsFragment, initialized to
     * show the text at 'index'.
     */
    public static HomeFragment newInstance(int index) {
        HomeFragment f = new HomeFragment();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);

        return f;
    }


    @BindView(R.id.listItems)
    ListView listView;
    @BindView(R.id.addItems)
    FloatingActionButton addItems;
    @BindView(R.id.proceed)
    RelativeLayout proceed;
    ItemsAdapter itemsAdapter;
    String[] itemsName = new String[]{"ARIEL", "SUSHI", "PUMPKIN", "COCO"};
    ArrayList<Integer> fishes = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_fragment, container, false);
        ButterKnife.bind(this, view);
        initViews(view);

        fishes.add(R.mipmap.dummy1);
        fishes.add(R.mipmap.dummy2);
        fishes.add(R.mipmap.dummy3);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllDetails();
    }

    List<FishDetailsMOdel.DetailsBean> data = new ArrayList<>();

    private void getAllDetails() {
        progress.show();
        apiInterface.getAllDetails().enqueue(new Callback<FishDetailsMOdel>() {
            @Override
            public void onResponse(Call<FishDetailsMOdel> call, Response<FishDetailsMOdel> response) {
                if (progress.isShowing()) {
                    progress.cancel();
                }
                if (response.isSuccessful()) {
                    if (response.body().getResult().equals("Success")) {
                        data = response.body().getDetails();

                        itemsAdapter = new ItemsAdapter(getActivity(), data, appPrefes, HomeFragment.this);
                        listView.setAdapter(itemsAdapter);
                    } else {
                        listView.setAdapter(null);
                    }
                }
            }

            @Override
            public void onFailure(Call<FishDetailsMOdel> call, Throwable t) {
                if (progress.isShowing()) {
                    progress.cancel();
                }
            }
        });

    }

    private void initViews(View view) {

        addItems.setOnClickListener(this);
        if (appPrefes.getDataBoolean(Constants.IS_ADMIN)) {
            proceed.setVisibility(View.GONE);
            addItems.setVisibility(View.VISIBLE);
        } else {
            proceed.setVisibility(View.GONE);
            addItems.setVisibility(View.GONE);
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ArrayList<String> images = new ArrayList<>();
                if (!data.get(i).getImage1().equals("http://chaakara.in/api/item/logo_white.png"))
                    images.add(data.get(i).getImage1());
                if (!data.get(i).getImage2().equals("http://chaakara.in/api/item/logo_white.png"))
                    images.add(data.get(i).getImage2());
                if (!data.get(i).getImage3().equals("http://chaakara.in/api/item/logo_white.png"))
                    images.add(data.get(i).getImage3());
                if (!data.get(i).getImage4().equals("http://chaakara.in/api/item/logo_white.png"))
                    images.add(data.get(i).getImage4());

                if (images.size() > 0) {
                    Intent intent = new Intent(getActivity(), FullScreenSlider.class);
                    intent.putExtra("position", 0);
                    intent.putExtra("imagelist", images);
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.addItems) {
            startActivity(new Intent(getActivity(), AddItemNew.class));
        }
    }

    String fishId_Enquiry = "";

    @Override
    public void onClickEnquiry(String fish_id) {
        fishId_Enquiry = fish_id;
        showAlertDialog(getActivity(), "Enquiry", "Your Enquiry will be sending to one of our excecutives. Are you sure You Want to continue?",
                "OK", "CANCEL", ENQUIRY);

    }

    private void callEnquiry() {
        progress.show();
        apiInterface.enquiry(appPrefes.getData(Constants.USER_ID), fishId_Enquiry).enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                if (progress.isShowing())
                    progress.cancel();
                if (response.isSuccessful()) {
                    if (response.body().getResult().equals("Success")) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                if (progress.isShowing())
                    progress.cancel();

            }
        });
    }

    @Override
    public void EditItems(FishDetailsMOdel.DetailsBean item) {
        Intent intent = new Intent(getActivity(), AddItemNew.class);
        intent.putExtra("fishItem", item);
        startActivity(intent);

    }

    @Override
    public void deleteItem(String itemId) {
        progress.show();
        apiInterface.deleteFish(itemId).enqueue(new Callback<FishDeleteModel>() {
            @Override
            public void onResponse(Call<FishDeleteModel> call, Response<FishDeleteModel> response) {
                if (progress.isShowing())
                    progress.cancel();

                if (response.isSuccessful()) {
                    if (response.body().isResult()) {
                        showRequestSuccessDialog(getActivity(), "Success", "Deleted Successfully", "OK", 12);
                        getAllDetails();

//                        itemsAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<FishDeleteModel> call, Throwable t) {
                if (progress.isShowing())
                    progress.cancel();
            }
        });
    }

    @Override
    public void onClickAlertOkButton(int apiCode) {
        super.onClickAlertOkButton(apiCode);
        if (apiCode == ENQUIRY) {
            callEnquiry();
        } else if (apiCode == 12) {

        }

    }

    private void call(String number) {


        String msgText = "New Enquiry has been Requested by " + appPrefes.getData(Constants.USER_NAME) + " Call on: " + appPrefes.getData(Constants.PHONE_USER)
                + "\n\n--Chaakara-App-Enquiries--";
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, msgText, null, null);

    }

}
