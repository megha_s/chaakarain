package chaakara.com.freshfish.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseFragment;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.responsemodels.RatingResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static chaakara.com.freshfish.commons.Constants.SUCCESS_FEEDBACK;

public class RatingAndFeedBack extends BaseFragment {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.feedback_et)
    EditText feedback_et;
    @BindView(R.id.submit_bt)
    Button submit_bt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_rating, container, false);
        ButterKnife.bind(this, view);

        submit_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int rating = Math.round(ratingBar.getRating());
                sendFeedback(feedback_et.getText().toString(), rating);
            }
        });
        return view;
    }

    public void sendFeedback(String feedback, int rating) {
        try {
            progress.show();
            apiInterface.sendFeedback(appPrefes.getData(Constants.USER_ID), rating + "", feedback)
                    .enqueue(new Callback<RatingResponse>() {
                        @Override
                        public void onResponse(Call<RatingResponse> call, Response<RatingResponse> response) {
                            if (progress.isShowing())
                                progress.cancel();

                            if (response.isSuccessful()) {
                                if (response.body().getResult().equals("Success")) {
                                    feedback_et.setText("");
                                    showRequestSuccessDialog(getActivity(), "Success", "Thank you for your feedback", "OK", SUCCESS_FEEDBACK);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<RatingResponse> call, Throwable t) {
                            if (progress.isShowing())
                                progress.cancel();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
