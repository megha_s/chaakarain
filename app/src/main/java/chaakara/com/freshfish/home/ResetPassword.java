package chaakara.com.freshfish.home;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseActivity;
import chaakara.com.freshfish.databinding.LayoutResetBinding;
import chaakara.com.freshfish.responsemodels.ForgotPassword;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassword extends BaseActivity {
    LayoutResetBinding binding;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_reset);
        binding.resetBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.otpEt.getText().toString().length() > 0 && (binding.passwordEt.getText().toString().length() > 0) &&
                        (binding.retypepasswordEt.getText().toString().length() > 0) && (binding.phoneEt.getText().toString().length() > 0)) {
                    if (binding.retypepasswordEt.getText().toString().equals(binding.passwordEt.getText().toString())) {
                        callApi();
                    } else {
                        Toast.makeText(ResetPassword.this, "Password mismatch", Toast.LENGTH_LONG).show();

                    }
                } else {
                    Toast.makeText(ResetPassword.this, "Fill All the fields", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /*   @Nullable
       @Override
       public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
           binding = DataBindingUtil.inflate(inflater, R.layout.layout_reset, container, false);

           return binding.getRoot();
       }
   */
    private void callApi() {
        progress.show();
        apiInterface.changePassword(binding.phoneEt.getText().toString(), binding.otpEt.getText().toString(), binding.passwordEt.getText().toString())
                .enqueue(new Callback<ForgotPassword>() {
                    @Override
                    public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                        if (progress.isShowing()) {
                            progress.cancel();
                        }
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {
                                Toast.makeText(ResetPassword.this, "Login with new password", Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotPassword> call, Throwable t) {
                        if (progress.isShowing()) {
                            progress.cancel();
                        }
                    }
                });

    }
}
