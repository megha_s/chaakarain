package chaakara.com.freshfish.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseFragment;
import chaakara.com.freshfish.responsemodels.RatingResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewRatingAndFeedback extends BaseFragment {

    /**
     * Create a new instance of DetailsFragment, initialized to
     * show the text at 'index'.
     */
    public static ViewRatingAndFeedback newInstance(int index) {
        ViewRatingAndFeedback f = new ViewRatingAndFeedback();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);

        return f;
    }

    @BindView(R.id.list)
    ListView ratingList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_rating_and_feedbaack, container, false);
        ButterKnife.bind(this, view);

        getRating();
        return view;
    }

    private void getRating() {
        try {
            progress.show();
            apiInterface.viewFeedback().enqueue(new Callback<RatingResponse>() {
                @Override
                public void onResponse(Call<RatingResponse> call, Response<RatingResponse> response) {
                    if (progress.isShowing())
                        progress.cancel();
                    if (response.isSuccessful()) {
                        if (response.body().getResult().equals("Success")) {
                            List<RatingResponse.DetailsBean> data = response.body().getDetails();
                            FeedbackAdapter adapter = new FeedbackAdapter(getActivity(), data, appPrefes);
                            ratingList.setAdapter(adapter);

                        }
                    }
                }

                @Override
                public void onFailure(Call<RatingResponse> call, Throwable t) {
                    if (progress.isShowing())
                        progress.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
