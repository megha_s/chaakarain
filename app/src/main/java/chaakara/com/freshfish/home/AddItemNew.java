package chaakara.com.freshfish.home;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.models.Image;
import com.makeramen.roundedimageview.RoundedImageView;
import com.yalantis.ucrop.UCrop;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import chaakara.com.freshfish.GetFilePath;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseActivity;
import chaakara.com.freshfish.responsemodels.UserRegistrationModel;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES;
import static com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT;
import static com.darsh.multipleimageselect.helpers.Constants.REQUEST_CODE;
import static chaakara.com.freshfish.commons.Constants.ITEM_ADDED_SUCCESSFULLY;

public class AddItemNew extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.image_add_pic_1)
    LinearLayout image_add_pic_1;
    @BindView(R.id.image_add_pic_2)
    LinearLayout image_add_pic_2;
    @BindView(R.id.image_add_pic_3)
    LinearLayout image_add_pic_3;
    @BindView(R.id.image_add_pic_4)
    LinearLayout image_add_pic_4;

    @BindView(R.id.pic1)
    RoundedImageView pic1;
    @BindView(R.id.pic2)
    RoundedImageView pic2;
    @BindView(R.id.pic3)
    RoundedImageView pic3;
    @BindView(R.id.pic4)
    RoundedImageView pic4;

    @BindView(R.id.addItem)
    LinearLayout addItem;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.name_et)
    EditText name_et;
    @BindView(R.id.perkg_checkbox)
    CheckBox perkgCheckbox;
    @BindView(R.id.perpiece_checkbox)
    CheckBox perpieceCheckbox;

    @BindView(R.id.delete1)
    ImageView delete1;
    @BindView(R.id.delete2)
    ImageView delete2;
    @BindView(R.id.delete3)
    ImageView delete3;
    @BindView(R.id.delete4)
    ImageView delete4;

    GetFilePath getFilePath;
    String currentImageString = "";
    public static int REQUEST_PICTURE_FROM_CAMERA = 2;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.layout_add_items_to_market);
        ButterKnife.bind(this);
        image_add_pic_1.setOnClickListener(this);
        image_add_pic_2.setOnClickListener(this);
        image_add_pic_3.setOnClickListener(this);
        image_add_pic_4.setOnClickListener(this);
        delete1.setOnClickListener(this);
        delete2.setOnClickListener(this);
        delete3.setOnClickListener(this);
        delete4.setOnClickListener(this);
        addItem.setOnClickListener(this);
        back.setOnClickListener(this);
        getFilePath = new GetFilePath(this);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_add_pic_1:
                currentImageString = "pic1";
                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemNew.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemNew.this);
                } else {
                    callChooser();
                }
                break;
            case R.id.image_add_pic_2:
                currentImageString = "pic2";
                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemNew.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemNew.this);
                } else {
                    callChooser();
                }
                break;
            case R.id.image_add_pic_3:
                currentImageString = "pic3";
                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemNew.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemNew.this);
                } else {
                    callChooser();
                }
                break;
            case R.id.image_add_pic_4:
                currentImageString = "pic4";
                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemNew.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemNew.this);
                } else {
                    callChooser();
                }
                break;
            case R.id.addItem:
                if (name_et.getText().toString().length() > 0 && ((image1 != null) || (image2 != null) || (image3 != null) || (image4 != null))) {

                    if (perkgCheckbox.isChecked()) {
                        addItemToCart(name_et.getText().toString(), image1, image2, image3, image4, "per Kg");

                    } else if (perpieceCheckbox.isChecked()) {
                        addItemToCart(name_et.getText().toString(), image1, image2, image3, image4, "Per Piece");

                    }

                } else {
                    Toast.makeText(this, "Fill the details", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.back:
                finish();
                break;
            case R.id.delete1:
                pic1.setImageBitmap(null);
                image_add_pic_1.setVisibility(View.VISIBLE);
                delete1.setVisibility(View.GONE);
                break;
            case R.id.delete2:
                pic2.setImageBitmap(null);
                image_add_pic_2.setVisibility(View.VISIBLE);
                delete2.setVisibility(View.GONE);
                break;
            case R.id.delete3:
                pic3.setImageBitmap(null);
                image_add_pic_3.setVisibility(View.VISIBLE);
                delete3.setVisibility(View.GONE);
                break;
            case R.id.delete4:
                pic4.setImageBitmap(null);
                image_add_pic_4.setVisibility(View.VISIBLE);
                delete4.setVisibility(View.GONE);

                break;
        }
    }


    public void callChooser() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose From");
        builder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhotoWithCamera();
            }
        });
        builder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectImageFromGallery();
            }
        });
        builder.show();
    }

    private void selectImageFromGallery() {
        Intent intent = new Intent(this, AlbumSelectActivity.class);
        intent.putExtra(INTENT_EXTRA_LIMIT, 1); // set limit for image selection
        startActivityForResult(intent, REQUEST_CODE);
    }


    private void takePhotoWithCamera() {

        File file = new File(android.os.Environment.getExternalStorageDirectory() + File.separator + currentImageString + ".jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        } else {
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_PICTURE_FROM_CAMERA);
        }

    }

    private File image1, image2, image3, image4;
    private String selectedImagePath = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(INTENT_EXTRA_IMAGES);
            try {
                System.gc();
                Uri selectedImageUri = Uri.fromFile(new File(images.get(0).path));
                selectedImagePath = getFilePath.getPath(selectedImageUri);
                image1 = new File(selectedImagePath);
                System.out.println("Image Path : " + image1);
                UCrop.Options options = new UCrop.Options();
                options.setLogoColor(getResources().getColor(R.color.colorPrimary));
                options.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                options.setActiveWidgetColor(getResources().getColor(R.color.colorPrimary));
                options.setCropFrameColor(getResources().getColor(R.color.colorPrimary));
                options.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + currentImageString + "imgCrop.jpg");
                UCrop.of(Uri.fromFile(image1), Uri.fromFile(file))
                        .withOptions(options)
                        .withAspectRatio(1, 1)
                        .withMaxResultSize(600, 600)
                        .start(AddItemNew.this);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if ((resultCode == Activity.RESULT_OK) && (requestCode == REQUEST_PICTURE_FROM_CAMERA)) {
            File getImage = new File(android.os.Environment.getExternalStorageDirectory() + File.separator + currentImageString + ".jpg");
            UCrop.Options options = new UCrop.Options();
            options.setFreeStyleCropEnabled(false);

            UCrop.of(Uri.fromFile(getImage), Uri.fromFile(getImage))
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(800, 800)
                    .start(this);

        } else if (resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            image1 = new File(getFilePath.getPath(resultUri));
            if (!image1.exists()) {
                new File(this.getFilesDir(), getFilePath.getPath(resultUri)).mkdirs();
            }
            Bitmap photo = decodeUriAsBitmap(resultUri);
            setImage(photo);
            try {
                System.gc();
                selectedImagePath = getFilePath.getPath(resultUri);
                switch (currentImageString) {
                    case "pic1":
                        image1 = new File(selectedImagePath);
                        break;
                    case "pic2":
                        image2 = new File(selectedImagePath);
                        break;
                    case "pic3":
                        image3 = new File(selectedImagePath);
                        break;
                    case "pic4":
                        image4 = new File(selectedImagePath);
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }

    private void setImage(Bitmap photo) {
        switch (currentImageString) {
            case "pic1":
                pic1.setImageBitmap(photo);
                break;
            case "pic2":
                pic2.setImageBitmap(photo);
                break;
            case "pic3":
                pic3.setImageBitmap(photo);
                break;
            case "pic4":
                pic4.setImageBitmap(photo);
                break;

        }

    }

    private Bitmap decodeUriAsBitmap(Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

    private void addItemToCart(String fishName, File image1, File image2, File image3, File image4, String quantityIn) {
        try {
            Map<String, RequestBody> map = new HashMap<>();
            if (image1 != null) {
                RequestBody file = RequestBody.create(MediaType.parse("image/*"), image1);
                map.put("image1\"; filename=\"ProfilePic_1" + Calendar.getInstance().getTimeInMillis() + ".jpg\" ", file);
            }
            if (image2 != null) {
                RequestBody file = RequestBody.create(MediaType.parse("image/*"), image2);
                map.put("image2\"; filename=\"ProfilePic_1" + Calendar.getInstance().getTimeInMillis() + ".jpg\" ", file);
            }
            if (image3 != null) {
                RequestBody file = RequestBody.create(MediaType.parse("image/*"), image3);
                map.put("image3\"; filename=\"ProfilePic_1" + Calendar.getInstance().getTimeInMillis() + ".jpg\" ", file);
            }
            if (image4 != null) {
                RequestBody file = RequestBody.create(MediaType.parse("image/*"), image4);
                map.put("image4\"; filename=\"ProfilePic_1" + Calendar.getInstance().getTimeInMillis() + ".jpg\" ", file);
            }
            RequestBody nameRQ = RequestBody.create(MediaType.parse("text/plain"), fishName);
            map.put("fish_name", nameRQ);

            RequestBody quantityrq = RequestBody.create(MediaType.parse("text/plain"), quantityIn);
            map.put("quantity", quantityrq);

            progress.show();
            apiInterface.addFish(map).enqueue(new Callback<UserRegistrationModel>() {
                @Override
                public void onResponse(Call<UserRegistrationModel> call, Response<UserRegistrationModel> response) {
                    if (progress.isShowing()) {
                        progress.cancel();
                        showRequestSuccessDialog("Success", "Item Added Successfully", "OK", ITEM_ADDED_SUCCESSFULLY);

                        finish();
                    }
                }

                @Override
                public void onFailure(Call<UserRegistrationModel> call, Throwable t) {
                    if (progress.isShowing()) {
                        progress.cancel();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}


