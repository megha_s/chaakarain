package chaakara.com.freshfish.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.AppPreferences;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.responsemodels.FishDetailsMOdel;


/**
 * Created by Lijo Mathew Theckanal on 20-Aug-18.
 */
public class ItemsAdapter extends ArrayAdapter<FishDetailsMOdel.DetailsBean> {

    public ItemsAdapter(Context context, List<FishDetailsMOdel.DetailsBean> data, AppPreferences appPreferences, HomeFragment cliclCalls) {
        super(context, 0, data);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.appPreferences = appPreferences;
        this.cliclCalls = cliclCalls;
    }


    private static class ViewHolder {
        public final LinearLayout mainLayout;
        public final LinearLayout addTocart_lv;
        public final LinearLayout edit_item_tv;
        public final TextView enquiry;
        public final TextView fishName;
        public final ImageView image;

        private ViewHolder(LinearLayout mainLayout, LinearLayout addTocart_lv, LinearLayout edit_item_tv, TextView enquiry,
                           TextView fishName, ImageView image) {
            this.mainLayout = mainLayout;
            this.addTocart_lv = addTocart_lv;
            this.edit_item_tv = edit_item_tv;
            this.enquiry = enquiry;
            this.fishName = fishName;
            this.image = image;
        }

        public static ViewHolder create(LinearLayout mainLayout) {
            LinearLayout edit_item_tv = mainLayout.findViewById(R.id.edit_item_tv);
            LinearLayout addTocart_lv = mainLayout.findViewById(R.id.addTocart_lv);
            TextView enquiry = mainLayout.findViewById(R.id.enquiry);
            TextView fishName = mainLayout.findViewById(R.id.fishName);
            ImageView image = mainLayout.findViewById(R.id.image);
            return new ViewHolder(mainLayout, addTocart_lv, edit_item_tv, enquiry, fishName, image);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.layout_view_market_item, parent, false);
            vh = ViewHolder.create((LinearLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        if (appPreferences.getDataBoolean(Constants.IS_ADMIN)) {
            vh.enquiry.setText("Delete");
        } else {
            vh.enquiry.setText("Enquiry");

        }
        final FishDetailsMOdel.DetailsBean item = getItem(position);
        if (appPreferences.getDataBoolean(Constants.IS_ADMIN)) {
            vh.edit_item_tv.setVisibility(View.GONE);
            vh.addTocart_lv.setVisibility(View.GONE);
        } else {
            vh.edit_item_tv.setVisibility(View.GONE);
            vh.addTocart_lv.setVisibility(View.GONE);
        }
        vh.enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appPreferences.getDataBoolean(Constants.IS_ADMIN)) {
                    cliclCalls.deleteItem(item.getFish_id());

                } else {
                    cliclCalls.onClickEnquiry(item.getFish_id());

                }
            }
        });
        vh.edit_item_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cliclCalls.EditItems(item);
            }
        });


        vh.fishName.setText(item.getFish_name() + "(" + item.getQuantity() + ")");
        if (item.getImage1().length() > 0 && !item.getImage1().equals("http://chaakara.in/api/item/logo_white.png"))
            Picasso.with(context)
                    .load(item.getImage1())
                    .into(vh.image);
        else if (item.getImage2().length() > 0 && !item.getImage2().equals("http://chaakara.in/api/item/logo_white.png"))
            Picasso.with(context)
                    .load(item.getImage2())
                    .into(vh.image);
        else if (item.getImage3().length() > 0 && !item.getImage3().equals("http://chaakara.in/api/item/logo_white.png"))
            Picasso.with(context)
                    .load(item.getImage3())
                    .into(vh.image);
        else if (item.getImage4().length() > 0 && !item.getImage4().equals("http://chaakara.in/api/item/logo_white.png"))
            Picasso.with(context)
                    .load(item.getImage4())
                    .into(vh.image);
        return vh.mainLayout;
    }

    private LayoutInflater mInflater;
    private Context context;
    AppPreferences appPreferences;
    cliclCalls cliclCalls;


    interface cliclCalls {
        public void onClickEnquiry(String fish_id);

        public void EditItems(FishDetailsMOdel.DetailsBean item);

        public void deleteItem(String itemId);
    }
}