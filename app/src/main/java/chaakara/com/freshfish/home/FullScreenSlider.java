package chaakara.com.freshfish.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseActivity;
import me.relex.circleindicator.CircleIndicator;

public class FullScreenSlider extends BaseActivity {
    int pos;
    ArrayList<String> images = new ArrayList<>();
    CustomPagerAdapter adapter;
    @BindView(R.id.pager)
    ViewPager imagepager;
    @BindView(R.id.close)
    ImageView close;
    CircleIndicator indicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.fullscreenslider);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            pos = getIntent().getIntExtra("position", 0);
            images = getIntent().getStringArrayListExtra("imagelist");
        }
        adapter = new CustomPagerAdapter(FullScreenSlider.this, images);
        imagepager.setAdapter(adapter);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(imagepager);
        imagepager.setCurrentItem(pos);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}
