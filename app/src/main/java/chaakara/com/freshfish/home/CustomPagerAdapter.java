package chaakara.com.freshfish.home;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.TouchImageView;

/**
 * Created by sics on 4/25/2017.
 */

class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> imagePaths = new ArrayList<>();

    public CustomPagerAdapter(Context context, ArrayList<String> imagePaths) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        TouchImageView imageView = (TouchImageView) itemView.findViewById(R.id.imageView);
//        imageView.setImageResource(imagePaths.get(position));
        Picasso.with(mContext).load(imagePaths.get(position))
//                .resize(150, 150)
                .placeholder(R.mipmap.loading)
                .error(R.mipmap.error)
                .into(imageView);
        container.addView(itemView);
        imageView.setMaxZoom(4f);


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}