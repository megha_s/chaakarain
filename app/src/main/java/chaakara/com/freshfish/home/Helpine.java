package chaakara.com.freshfish.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseFragment;
import chaakara.com.freshfish.commons.Constants;

public class Helpine extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_helpine, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.helpine1)
    public void call1() {
        call(Constants.HELPLINE_SHAMMAS);
    }

    @OnClick(R.id.helpine2)
    public void call2() {
        call(Constants.HELPLINE_AJMAL);
    }

  /*  @OnClick(R.id.helpine3)
    public void call3() {
        call(Constants.HELPLINE_AJMAL);
    }*/

    private void call(String number) {
       /* if (!permissionUtil.hasCallPermission(getActivity())) {
            permissionUtil.requestCallPermissions(getActivity(), 100);
        } else {*/
        /*String uri = "tel:" + number;
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
*/
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
//        }
    }
}
