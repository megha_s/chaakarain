package chaakara.com.freshfish.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.AppPreferences;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.responsemodels.RatingResponse;


/**
 * Created by Lijo Mathew Theckanal on 20-Aug-18.
 */
public class FeedbackAdapter extends ArrayAdapter<RatingResponse.DetailsBean> {

    public FeedbackAdapter(Context context, List<RatingResponse.DetailsBean> data, AppPreferences appPreferences) {
        super(context, 0, data);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.appPreferences = appPreferences;
    }


    private static class ViewHolder {
        public final LinearLayout mainLayout;
        public final TextView feedback_tv;
        public final TextView name_tv;
        public final TextView phone_tv;
        public final CircleImageView image;
        public final RatingBar ratingBar;

        private ViewHolder(LinearLayout mainLayout, TextView feedback_tv, RatingBar ratingBar,
                           TextView name_tv, CircleImageView image, TextView phone_tv) {
            this.mainLayout = mainLayout;
            this.feedback_tv = feedback_tv;
            this.name_tv = name_tv;
            this.image = image;
            this.ratingBar = ratingBar;
            this.phone_tv = phone_tv;
        }

        public static ViewHolder create(LinearLayout mainLayout) {
            LinearLayout addTocart_lv = mainLayout.findViewById(R.id.addTocart_lv);
            TextView feedback_tv = mainLayout.findViewById(R.id.feedback_tv);
            TextView name_tv = mainLayout.findViewById(R.id.name_tv);
            TextView phone_tv = mainLayout.findViewById(R.id.phone_tv);
            CircleImageView image = mainLayout.findViewById(R.id.dp);
            RatingBar ratingBar = mainLayout.findViewById(R.id.ratingBar);
            return new ViewHolder(mainLayout, feedback_tv, ratingBar, name_tv, image, phone_tv);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.layout_item_rating, parent, false);
            vh = ViewHolder.create((LinearLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        RatingResponse.DetailsBean item = getItem(position);

        vh.feedback_tv.setText(item.getFeedback());
        vh.name_tv.setText(item.getName());
        vh.phone_tv.setText(item.getPhone());
        vh.ratingBar.setNumStars(Integer.parseInt(item.getRating()));
        Picasso.with(context)
                .load(Constants.IMAGE_URL + item.getProfilePic())
                .error(R.drawable.default_pic)
                .placeholder(R.drawable.default_pic)
                .into(vh.image);
        return vh.mainLayout;
    }

    private LayoutInflater mInflater;
    private Context context;
    AppPreferences appPreferences;

}