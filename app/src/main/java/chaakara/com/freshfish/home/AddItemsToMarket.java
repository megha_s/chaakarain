package chaakara.com.freshfish.home;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import chaakara.com.freshfish.GetFilePath;
import chaakara.com.freshfish.R;
import chaakara.com.freshfish.commons.BaseActivity;
import chaakara.com.freshfish.commons.ImageInputHelper;
import chaakara.com.freshfish.responsemodels.FishDetailsMOdel;
import chaakara.com.freshfish.responsemodels.UserRegistrationModel;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static chaakara.com.freshfish.commons.Constants.ITEM_ADDED_SUCCESSFULLY;

public class AddItemsToMarket extends BaseActivity implements View.OnClickListener, ImageInputHelper.ImageActionListener {

    private ImageInputHelper imageInputHelper;
    private static final int REQUEST_PICTURE_FROM_GALLERY = 23;
    private static final int REQUEST_PICTURE_FROM_CAMERA = 24;
    private String currentImageSring = "";


    @BindView(R.id.image_add_pic_1)
    LinearLayout image_add_pic_1;
    @BindView(R.id.image_add_pic_2)
    LinearLayout image_add_pic_2;
    @BindView(R.id.image_add_pic_3)
    LinearLayout image_add_pic_3;
    @BindView(R.id.image_add_pic_4)
    LinearLayout image_add_pic_4;

    @BindView(R.id.pic1)
    RoundedImageView pic1;
    @BindView(R.id.pic2)
    RoundedImageView pic2;
    @BindView(R.id.pic3)
    RoundedImageView pic3;
    @BindView(R.id.pic4)
    RoundedImageView pic4;

    @BindView(R.id.addItem)
    LinearLayout addItem;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.name_et)
    EditText name_et;
    @BindView(R.id.perkg_checkbox)
    CheckBox perkgCheckbox;
    @BindView(R.id.perpiece_checkbox)
    CheckBox perpieceCheckbox;

    @BindView(R.id.delete1)
    ImageView delete1;
    @BindView(R.id.delete2)
    ImageView delete2;
    @BindView(R.id.delete3)
    ImageView delete3;
    @BindView(R.id.delete4)
    ImageView delete4;

    String currentImageString = "";
    private File image1, image2, image3, image4;
    FishDetailsMOdel.DetailsBean currentItem;

    GetFilePath getFilePath;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.layout_add_items_to_market);
        ButterKnife.bind(this);
        getFilePath=new GetFilePath(getApplicationContext());
        image_add_pic_1.setOnClickListener(this);
        image_add_pic_2.setOnClickListener(this);
        image_add_pic_3.setOnClickListener(this);
        image_add_pic_4.setOnClickListener(this);
        delete1.setOnClickListener(this);
        delete2.setOnClickListener(this);
        delete3.setOnClickListener(this);
        delete4.setOnClickListener(this);
        addItem.setOnClickListener(this);
        back.setOnClickListener(this);
        imageInputHelper = new ImageInputHelper(this);
        imageInputHelper.setImageActionListener(this);


        if (getIntent().hasExtra("fishItem")) {
            if (getIntent().getParcelableExtra("fishItem") != null) {
                currentItem = getIntent().getParcelableExtra("fishItem");
            }
        }
        if (currentItem != null) {

        }
    }


    @Override
    public void onSuccessDialogClick(int apiCode) {
        super.onSuccessDialogClick(apiCode);
        switch (apiCode) {
            case ITEM_ADDED_SUCCESSFULLY:
                finish();
                break;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_add_pic_1:
                currentImageString = "pic1";
                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemsToMarket.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemsToMarket.this);
                } else {
                    imageInputHelper.callChooser(currentImageString);
                }
                break;
            case R.id.image_add_pic_2:
                currentImageString = "pic2";

                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemsToMarket.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemsToMarket.this);
                } else {
                    imageInputHelper.callChooser(currentImageString);
                }
                break;
            case R.id.image_add_pic_3:
                currentImageString = "pic3";

                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemsToMarket.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemsToMarket.this);
                } else {
                    imageInputHelper.callChooser(currentImageString);
                }
                break;
            case R.id.image_add_pic_4:
                currentImageString = "pic4";

                if (permissionUtil.shouldNeedMultiplePermissionForProfile(AddItemsToMarket.this)) {
                    permissionUtil.requestMultiplePermissionsForProfile(AddItemsToMarket.this);
                } else {
                    imageInputHelper.callChooser(currentImageString);
                }
                break;
            case R.id.back:
                finish();
                break;
            case R.id.addItem:
                if (name_et.getText().toString().length() > 0 && ((image1 != null) || (image2 != null) || (image3 != null) || (image4 != null))) {

                    if (perkgCheckbox.isChecked()) {
                        addItemToCart(name_et.getText().toString(), image1, image2, image3, image4, "per Kg");

                    } else if (perpieceCheckbox.isChecked()) {
                        addItemToCart(name_et.getText().toString(), image1, image2, image3, image4, "Per Piece");

                    }

                } else {
                    Toast.makeText(this, "Fill the details", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.delete1:
                pic1.setImageBitmap(null);
                image_add_pic_1.setVisibility(View.VISIBLE);
                delete1.setVisibility(View.GONE);
                break;
            case R.id.delete2:
                pic2.setImageBitmap(null);
                image_add_pic_2.setVisibility(View.VISIBLE);
                delete2.setVisibility(View.GONE);
                break;
            case R.id.delete3:
                pic3.setImageBitmap(null);
                image_add_pic_3.setVisibility(View.VISIBLE);
                delete3.setVisibility(View.GONE);
                break;
            case R.id.delete4:
                pic4.setImageBitmap(null);
                image_add_pic_4.setVisibility(View.VISIBLE);
                delete4.setVisibility(View.GONE);

                break;
        }
    }

    private void addItemToCart(String fishName, File image1, File image2, File image3, File image4, String quantityIn) {
        Map<String, RequestBody> map = new HashMap<>();
        if (image1 != null) {
            RequestBody file = RequestBody.create(MediaType.parse("image/*"), image1);
            map.put("image1", file);
        }
        if (image2 != null) {
            RequestBody file = RequestBody.create(MediaType.parse("image/*"), image2);
            map.put("image2", file);
        }
        if (image3 != null) {
            RequestBody file = RequestBody.create(MediaType.parse("image/*"), image3);
            map.put("image3", file);
        }
        if (image4 != null) {
            RequestBody file = RequestBody.create(MediaType.parse("image/*"), image4);
            map.put("image4", file);
        }
        RequestBody nameRQ = RequestBody.create(MediaType.parse("text/plain"), fishName);
        map.put("fish_name", nameRQ);

        RequestBody quantityrq = RequestBody.create(MediaType.parse("text/plain"), quantityIn);
        map.put("quantity", quantityrq);

        progress.show();
        apiInterface.addFish(map).enqueue(new Callback<UserRegistrationModel>() {
            @Override
            public void onResponse(Call<UserRegistrationModel> call, Response<UserRegistrationModel> response) {
                if (progress.isShowing()) {
                    progress.cancel();
                    finish();
                    showRequestSuccessDialog("Success", "Item Added Successfully", "OK", ITEM_ADDED_SUCCESSFULLY);

                }
            }

            @Override
            public void onFailure(Call<UserRegistrationModel> call, Throwable t) {
                if (progress.isShowing()) {
                    progress.cancel();
                }
            }
        });

    }

    boolean isGalery = false;

  /*  @Override
    public void onImageSelectedFromGallery(Bitmap bitmap) {
        isGalery = true;
        setProfilePicfROM(null, bitmap);
    } */

    @Override
    public void onImageSelectedFromGallery(String imageUri) {
        isGalery = true;
        setProfilePicfROM(new File(imageUri), null);
    }

    @Override
    public void onImageTakenFromCamera(String imageFile) {
        isGalery = false;
        setProfilePicfROM(new File(imageFile), null);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        imageInputHelper.onActivityResult(requestCode, resultCode, data);
//        if ((resultCode == Activity.RESULT_OK) && (requestCode == REQUEST_PICTURE_FROM_CAMERA)) {
////                try {
//            File getImage = new File(Environment.getExternalStorageDirectory() + File.separator + currentImageSring + ".jpg");
////                    picUri = Uri.fromFile(getImage);
////                    Bitmap myBitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), picUri);
////                    myBitmap = rotateImageIfRequired(myBitmap, picUri);
////                    myBitmap = getResizedBitmap(myBitmap, 500);
////                    profilePic = savetoFile(myBitmap);
////                    imageActionListener.onImageTakenFromCamera(profilePic);
//            UCrop.Options options = new UCrop.Options();
//            options.setFreeStyleCropEnabled(false);
//
//            UCrop.of(Uri.fromFile(getImage), Uri.fromFile(getImage))
//                    .withAspectRatio(16, 9)
//                    .withMaxResultSize(800, 800)
//                    .start(AddItemsToMarket.this);
//
//        } if (requestCode == ConstantsCustomGallery.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
//            //The array list has the image paths of the selected images
//            ArrayList<Image> images = data.getParcelableArrayListExtra(ConstantsCustomGallery.INTENT_EXTRA_IMAGES);
//
//            for (int i = 0; i < images.size(); i++) {
////                Uri uri = Uri.fromFile(new File(images.get(i).path));
////                File f = new File(uri));
//                Uri uri = Uri.fromFile(new File(images.get(i).path));
//                onImageSelectedFromGallery(images.get(i).path);
//                // start play with image uri
//            }
//        }
//
//        /*else if ((resultCode == Activity.RESULT_OK) && (requestCode == REQUEST_PICTURE_FROM_GALLERY)) {
//            try {
//                picUri = data.getData();
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                Cursor cursor = mContext.getContentResolver().query(picUri,
//                        filePathColumn, null, null, null);
//                cursor.moveToFirst();
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                String picturePath = cursor.getString(columnIndex);
//                cursor.close();
//
////                imageActionListener.onImageSelectedFromGallery(BitmapFactory.decodeFile(picturePath));
//
//                File f = new File(picturePath);
//                imageActionListener.onImageSelectedFromGallery(f);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }*/ else if (resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
//            getFilePath = new GetFilePath(getApplicationContext());
//            final Uri resultUri = UCrop.getOutput(data);
//            onImageTakenFromCamera(getFilePath.getPath(resultUri));
//
//        } else if (resultCode == UCrop.RESULT_ERROR) {
//            final Throwable cropError = UCrop.getError(data);
//        }
    }


    public void setProfilePicfROM(File mProfilePic, Bitmap bitmapPic) {
        Bitmap bitmap = null;
        File mProfilePic_this = null;

        if (isGalery) {
            bitmap = bitmapPic;
            mProfilePic_this = savetoFile(bitmap);
        } else {
            mProfilePic_this = mProfilePic;
            String filePath = mProfilePic.getAbsolutePath();
            bitmap = BitmapFactory.decodeFile(filePath);
        }

        switch (currentImageString) {
            case "pic1":
                setImage(mProfilePic, pic1);
//                pic1.setImageBitmap(bitmap);
                image1 = mProfilePic_this;
                image_add_pic_1.setVisibility(View.GONE);
                break;
            case "pic2":
                setImage(mProfilePic, pic2);

//                pic2.setImageBitmap(bitmap);
                image2 = mProfilePic_this;
                image_add_pic_2.setVisibility(View.GONE);

                break;
            case "pic3":
                setImage(mProfilePic, pic3);

//                pic3.setImageBitmap(bitmap);
                image3 = mProfilePic_this;
                image_add_pic_3.setVisibility(View.GONE);

                break;
            case "pic4":
                setImage(mProfilePic, pic4);

//                pic4.setImageBitmap(bitmap);
                image4 = mProfilePic_this;
                image_add_pic_4.setVisibility(View.GONE);

                break;
        }
    }


    public void setImage(File file, RoundedImageView imageView) {
        Picasso.with(this).load(file).into(imageView);

    }

    public File savetoFile(Bitmap bitmap) {
        FileOutputStream ostream;
        File file = null;
        try {
            file = File.createTempFile("Edited", "png", getApplicationContext().getExternalCacheDir());
            file.createNewFile();
            ostream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ostream.flush();
            ostream.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return file;
    }
}
