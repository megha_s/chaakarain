package chaakara.com.freshfish;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;

import chaakara.com.freshfish.commons.BaseActivity;
import chaakara.com.freshfish.commons.Constants;
import chaakara.com.freshfish.logins.UserRegistration;

import static chaakara.com.freshfish.commons.Constants.PERMISSION_REQUEST_CODE;

public class Splash extends BaseActivity {
    public int SPLASH_TIME = 1 * 1000;// 4 seconds

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        checkPermissions();
    }

    private void checkPermissions() {
        if (permissionUtil.shouldNeedMultiplePermission(Splash.this)) {
            permissionUtil.requestMultiplePermissions(Splash.this);
        } else {
            delayFlow();
        }
    }


    private void delayFlow() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (appPrefes.getDataBoolean(Constants.IS_LOGIN)) {
                    Intent intent = new Intent(Splash.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(Splash.this, UserRegistration.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, SPLASH_TIME);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (permissionUtil.validateGrantedPermissions(grantResults)) {
                    delayFlow();
                } else {
                    delayFlow();
                }
                break;
        }
    }

}
